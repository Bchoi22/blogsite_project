<!DOCTYPE HTML>

<?php
session_start();
require 'database.php';

if (htmlspecialchars($_SESSION['username']) != NULL && htmlspecialchars($_SERVER['REQUEST_METHOD']) == 'POST'){
    $sess_token = $mysqli->real_escape_string($_SESSION['token']);
    $post_token = $mysqli->real_escape_string($_POST['token']);
    if(!hash_equals($sess_token, $post_token)){
	    die("Request forgery detected");
    }
    //$mysqli->query(/* perform transfer */);

    $commentPost=$mysqli->real_escape_string($_POST['comment']);
    if (isset($commentPost)){
        $id=htmlspecialchars($_SESSION['id']);
        $username=htmlspecialchars($_SESSION['username']);
        $storytitle=htmlspecialchars($_SESSION['storytitle']);
            
        $stmt = $mysqli->prepare("insert into commentary (username, story_title, comments, comment_id) values (?, ?, ?, ?)");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('ssss', $username, $storytitle, $commentPost, $id); 
        $stmt->execute();
        $stmt->close();

        $url=htmlspecialchars($_SERVER['HTTP_REFERER']);
        echo "Comment upload success!  Redirecting back...";
        header("Refresh: 1; URL=$url");
    }
}
?>